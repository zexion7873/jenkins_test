package com.thinkpower.springcloudstreamkafka.Service;

import org.springframework.stereotype.Service;

import com.thinkpower.springcloudstreamkafka.DTO.TestDTO;

@Service
public class MessageService {
	
	public boolean approval(TestDTO dto) {
		return !dto.getApproval().isEmpty();
	}
}

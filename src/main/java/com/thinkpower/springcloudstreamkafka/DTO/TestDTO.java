package com.thinkpower.springcloudstreamkafka.DTO;

public class TestDTO {
	
	private String approval;
	private String pass;
	private String buileUrl;
	
	

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getBuileUrl() {
		return buileUrl;
	}

	public void setBuileUrl(String buileUrl) {
		this.buileUrl = buileUrl;
	}
	
	
	
}
